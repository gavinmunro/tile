#! /usr/bin/env python3

"""
Read 10 lines of 10 characters from stdout(should be'stdin'?) as the TILE.
Extract the inner 8x8 tile as a hexadecimal (hex) representation of the DATA.
Extract from {N,E,S,W} hex bytes and combine into the CHECKSUM of 4 bytes.
Verify the CHECKSUM is the first 4 bytes of the SHA-256 of the DATA.
Confirm the MIDDLE DOTS are present.
Write the DATA to stdout as binary.
"""

import sys
import hashlib
from pathlib import Path


_base_dir = Path(__file__).absolute().parents[0]  # The current directory


def read_tile():
    """ Read 10 lines of 10 characters from stdout as the TILE. """
    t10x10 = []
    # with sys.stdin as f:   # ToDo: restore after testing
    with open(str(_base_dir / "10x10.tile"), 'r') as infile:
        for row in range(10):
            line = infile.readline()
            t10x10.append([line[col] for col in range(10)])
    return t10x10


if __name__ == "__main__":
    tile10x10 = read_tile()
    
    # Extract 8x8 tile
    tile8x8 = [['' for _ in range(8)] for _ in range(8)]
    for i in range(8):
        for j in range(8):
            tile8x8[i][j] = tile10x10[i + 1][j + 1]

    data_hex = ''.join(tile8x8[i][j] for i in range(8) for j in range(8))

    # Get checksum4 from {N, E, S, W} elements of 10x10 tile
    checksum4 = [['' for _ in range(2)] for _ in range(4)]
    checksum4[0][0] = tile10x10[0][4]
    checksum4[0][1] = tile10x10[0][5]

    checksum4[3][0] = tile10x10[4][0]
    checksum4[1][0] = tile10x10[4][9]
    checksum4[3][1] = tile10x10[5][0]
    checksum4[1][1] = tile10x10[5][9]

    checksum4[2][0] = tile10x10[9][4]
    checksum4[2][1] = tile10x10[9][5]
    
    # Get checksum as array of len 4 of 2-digit elements.
    checksum = [checksum4[n][0] + checksum4[n][1] for n in range(4)]
    checksum = ''.join(checksum)  # Mutate to concatentation.
    checksum = bytes.fromhex(checksum)
    
    # Verify the CHECKSUM is the first 4 bytes of the SHA-256 of the DATA
    data = bytes.fromhex(data_hex)
    hsh = hashlib.sha256(data).digest()[:4]
    assert hsh == checksum

    # Confirm middle-dot's are present in tile10x10
    dot = '\u00B7'
    colln = []
    colln.extend([tile10x10[0][0], tile10x10[0][1], tile10x10[0][2], tile10x10[0][3]])
    colln.extend([tile10x10[0][6], tile10x10[0][7], tile10x10[0][7], tile10x10[0][8]])
    colln.extend([tile10x10[1][0], tile10x10[1][9]])
    colln.extend([tile10x10[2][0], tile10x10[2][9]])
    colln.extend([tile10x10[3][0], tile10x10[3][9]])

    colln.extend([tile10x10[6][0], tile10x10[6][9]])
    colln.extend([tile10x10[7][0], tile10x10[7][9]])
    colln.extend([tile10x10[8][0], tile10x10[8][9]])
    colln.extend([tile10x10[9][0], tile10x10[9][1], tile10x10[9][2], tile10x10[9][3]])
    colln.extend([tile10x10[9][6], tile10x10[9][7], tile10x10[9][7], tile10x10[9][8]])
    for c in colln:
        assert c == dot
    
    # Write DATA to stdout as binary
    sys.stdout.buffer.write(data)
    
    # cat 10x10.tile | python3 -m elit | cmp - 32.bin
