#! /usr/bin/env python3

"""
Read 32 bytes from stdin as the DATA.  Take the SHA-256 of the DATA as the HASH.
Take the most significant 4 bytes of the HASH as the CHECKSUM.
Convert the DATA and CHECKSUM to hexadecimal (hex) representations.
Arrange the DATA hex into an 8x8 tile.
Place the 8x8 tile in the centre of a 10x10 tile.
Split the CHECKSUM hex into 4 and arrange at {N,E,S,W} around the 8x8 tile.
Fill the remainder of the tile with MIDDLE DOT (U+00B7).
The 10x10 tile is the TILE.
Write the TILE to stdout.
"""

import sys
import hashlib
from pathlib import Path

_base_dir = Path(__file__).absolute().parents[0]  # The current directory


def read_32b():
    """ DATA = read 32 bytes from stdin """
    # byte_list = []
    # with sys.stdin:  # ToDo: restore after testing
    #     for _ in range(32):
    #         byte = sys.stdin.read(1)
    #         byte_list.append(byte)
    # return byte_list
    with open(str(_base_dir / "32.bin"), 'rb') as f:
        bytes32 = f.read(32)
    return bytes32


dot = '\u00B7'
tile10x10 = [[dot for _ in range(10)] for _ in range(10)]


if __name__ == "__main__":
    read_32b()
    data = read_32b()
    data_hex = data.hex()
    
    hsh = hashlib.sha256(data).digest()
    checksum = hsh[:4]  # Take most significant 4 bytes
    ch = checksum.hex()
    checksum4 = [ch[0:2], ch[2:4], ch[4:6], ch[6:8]]
    # print(checksum4)  # debug
    
    tile8x8 = [[data_hex[j * 8 + i] for i in range(8)] for j in range(8)]

    # Place 8x8 tile in centre of tile10x10
    for i in range(8):
        for j in range(8):
            tile10x10[i + 1][j + 1] = tile8x8[i][j]

    # Arrange checksum {N, E, S, W}
    tile10x10[0][4] = checksum4[0][0]
    tile10x10[0][5] = checksum4[0][1]

    tile10x10[4][0] = checksum4[3][0]
    tile10x10[4][9] = checksum4[1][0]
    tile10x10[5][0] = checksum4[3][1]
    tile10x10[5][9] = checksum4[1][1]

    tile10x10[9][4] = checksum4[2][0]
    tile10x10[9][5] = checksum4[2][1]

    # for row in range(10):
    #     print(tile10x10[row])  # debug
    
    # Write tile to stdout
    with sys.stdout as f:
        for row in range(10):
            for col in range(10):
                f.write(tile10x10[row][col])
            f.write('\n')

    # cat 32.bin | python3 -m tile | diff - 10x10.tile
